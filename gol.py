#!/usr/bin/python3

import time
import pygame
from random import randint

pixelSize = 5
white = (255, 255, 255)
black = (0, 0, 0)

width = 300
height = 200

screenSize = int((width * height) / (pixelSize * pixelSize))


def main():
    z = lambda x, y: int(width / pixelSize * y + x)
    a = lambda idx: int((idx * pixelSize) % width / pixelSize)
    b = lambda idx: int(idx * pixelSize / width)
    c = lambda x, y: [(x, y - 1), (x, y + 1), (x - 1, y), (x + 1, y), (x - 1, y - 1), (x - 1, y + 1), (x + 1, y - 1), (x + 1, y + 1)]
    d = lambda ds, x, y: 0 if x < 0 or y < 0 or x >= width / pixelSize or y >= height / pixelSize else 1 if ds[z(x, y)] else 0
    e = lambda ds, l, idx: (l[idx] == 3) if not ds[idx] else (l[idx] == 2 or l[idx] == 3)

    screen = pygame.display.set_mode((width, height))
    pygame.display.set_caption('Gol')

    data_set = list([True if randint(0, 1) else False for _ in range(screenSize)])
    running = True

    while running:
        # render like a babouin so no one get what's happening
        [screen.fill(black if data_set[i] == 0 else white, ((a(i) * pixelSize, b(i) * pixelSize), (pixelSize, pixelSize))) for i in range(screenSize)]
        pygame.display.flip()

        # Amen
        tmp = list([sum([d(data_set, elem[0], elem[1]) for elem in c(a(i),b(i))]) for i in range(screenSize)])
        data_set = list([e(data_set, tmp, i) for i in range(screenSize)])

        time.sleep(0.1)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False


main()
